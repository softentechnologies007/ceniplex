#h {
  text-align: center;
  font-family: sans-serif;
  font-weight: bolder;
}

.images {
  width: 1200px;
  grid-template-columns: 1fr 1fr 1fr 1fr;
  gap: 3rem;
  margin: 0 auto;
  display: grid;
  position: relative;
}

.img {
  height: 100%;
  width: 100%;
  aspect-ratio: 3 / 4;
  object-fit: cover;
}

#h1 {
  font-family: sans-serif;
  font-size: large;
  position: relative;
}

/*# sourceMappingURL=index.630d62f4.css.map */
