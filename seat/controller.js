import Model from "./model";

class Controller{
    constructor(){
        this.model = new Model; 
    }
    getseats() {
        return this.model.getseats();
    }
    setseat() {
        this.model.setseat();
    }
}
export default Controller;